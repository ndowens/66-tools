# Contributor    : Eric Vidal <eric@obarun.org>
# Contributor    : Jean-Michel T.Dydak <jean-michel@obarun.org>
# Maintainer	 : Nathan <ndowens@artixlinux.org>
# PkgSource     : https://framagit.org/pkg/obcore/66-tools
#-----------------------------------------------------------------------------------------------

#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgdesc="small tools and helpers for service scripts execution"

pkgname=66-tools

pkgver=0.0.7.2
pkgrel=1

url="https://framagit.org/Obarun/66-tools.git"

track=tag
target="v${pkgver}"
source=(
    "${pkgname}::git+${url}#${track}=${target}"
)

#-------------------------------------| BUILD CONFIGURATION |-----------------------------------

makedepends=(
    'git'
    'skalibs>=2.10.0.0'
    '66-libs>=0.1.2.0'
    'execline>=2.7.0.0'
    'lowdown')

#--------------------------------------------| BUILD |-------------------------------------------

build() {
    cd "${pkgname}"

    ./configure \
        --bindir=/usr/bin \
        --disable-shared \
        --with-lib=/usr/lib/skalibs \
        --with-lib=/usr/lib/execline \
        --with-lib=/usr/lib/oblibs \
        --with-ns-rule=/etc/66/script/ns

    make
}

#-------------------------------------------| PACKAGE |------------------------------------------

package() {
    cd "${pkgname}"

    make DESTDIR="${pkgdir}" install install-ns-rule

    install -Dm 0644 LICENSE "${pkgdir}"/usr/share/licenses/"${pkgname}"/LICENSE
}

#------------------------------------| INSTALL CONFIGURATION |----------------------------------

arch=('x86_64')

groups=(
    'base'
    '66-init'
)

depends=(
    'skalibs>=2.10.0.0'
    '66-libs>=0.1.2.0'
    'execline>=2.7.0.0'
)

#-------------------------------------| SECURITY AND LICENCE |----------------------------------

sha512sums=('SKIP')
license=('ISC')
